BaseTFPlayer.BloodSportKiller // Player killed enemy under Adrenaline Rush powerup and in rampage
BaseTFPlayer.StartDeploying // Probably something related to vehicle deployment
BaseTFPlayer.StartUnDeploying// Probably something related to vehicle undeployment
BaseTFPlayer.KnockedDown // Player has been knocked down to the ground for a few seconds by Commando with boot, or by "kd" clientcommand
BaseTFPlayer.ThermalOn // Infiltrator used "special" clientcommand
BaseTFPlayer.ThermalOff // Infiltrator used "special" clientcommand
BaseTFPlayer.PickupResources // Player successfully gathered resource chunks
BaseTFPlayer.DonateResources // Player gave the other player some amount of it's resources
BaseTFPlayer.Rage // Adrenalin Rush powerup start sound
BaseTFPlayer.HeavyBreathing // Adrenalin Rush powerup start sound, which stops after powerup ends
BaseCombatCharacter.EMPPulse // Technician's EMP powerup start sound
BaseObject.SapperDestroyingTeamBuilding // Team player's building has been sapped
+ResourceChunk.Pickup // Resource chunk picked up by player or vehicle
BaseTFFourWheelVehicle.EMP // When a vehicle has EMP powerup and trying to drive with it (plays every 2 seconds)
+BaseTFFourWheelVehicle.RamSound // Vehicle has been hit by another team
+SignalFlare.Burn = items/flare/burn.wav // Signal flare is spawned
+SignalFlare.BurnLower ?= items/flare/burn.wav but lower volume // Signal flare fading away
+SignalFlare.Impact = items/flare/impact.wav // Signal flare impact sound on the first surface hit
+TFTeam.CapturedZone // Resource Zone has been added to the zone list of zones
+TFTeam.LostZone // Resource Zone has been removed from the team
+TFTeam.ObtainStolenTechnology ?= vox/stolen-tech-received.wav // A new stolen technology has been attained by the team (not being used)
+TFTeam.BoughtPreferredTechnology ?= vox/voted-for-tech-bought.wav // Tell the player his preferred tech has been bought
+TFTeam.AddOrder // "New Order Received!"
+WeaponObjectSapper.Attach // Player started attaching the Sapper
+WeaponObjectSapper.AttachFail // Something went wrong while attaching the Sapper

+Human.Default.StepLeft
+Human.Default.StepRight
+Human.SolidMetal.StepLeft
+Human.SolidMetal.StepRight
+Human.Dirt.StepLeft
+Human.Dirt.StepRight
+Human.Mud.StepLeft
+Human.Mud.StepRight
+Human.Grass.StepLeft
+Human.Grass.StepRight
+Human.MetalGrate.StepLeft
+Human.MetalGrate.StepRight
+Human.MetalVent.StepLeft
+Human.MetalVent.StepRight
+Human.Tile.StepLeft
+Human.Tile.StepRight
+Human.Wood.StepLeft
+Human.Wood.StepRight
+Human.WoodPanel.StepLeft
+Human.WoodPanel.StepRight
+Human.Water.StepLeft
+Human.Water.StepRight
+Human.Wade.StepLeft
+Human.Wade.StepRight
+Human.Ladder.StepLeft
+Human.Ladder.StepRight
+Human.Concrete.StepLeft
+Human.Concrete.StepRight
+Human.Gravel.StepLeft
+Human.Gravel.StepRight
+Human.ChainLink.StepLeft
+Human.ChainLink.StepRight
+Human.Sand.StepLeft
+Human.Sand.StepRight
+Alien.Default.StepLeft
+Alien.Default.StepRight
+Alien.SolidMetal.StepLeft
+Alien.SolidMetal.StepRight
+Alien.Dirt.StepLeft
+Alien.Dirt.StepRight
+Alien.Mud.StepLeft
+Alien.Mud.StepRight
+Alien.Grass.StepLeft
+Alien.Grass.StepRight
+Alien.MetalGrate.StepLeft
+Alien.MetalGrate.StepRight
+Alien.MetalVent.StepLeft
+Alien.MetalVent.StepRight
+Alien.Tile.StepLeft
+Alien.Tile.StepRight
+Alien.Wood.StepLeft
+Alien.Wood.StepRight
+Alien.WoodPanel.StepLeft
+Alien.WoodPanel.StepRight
+Alien.Water.StepLeft
+Alien.Water.StepRight
+Alien.Wade.StepLeft
+Alien.Wade.StepRight
+Alien.Ladder.StepLeft
+Alien.Ladder.StepRight
+Alien.Concrete.StepLeft
+Alien.Concrete.StepRight
+Alien.Gravel.StepLeft
+Alien.Gravel.StepRight
+Alien.ChainLink.StepLeft
+Alien.ChainLink.StepRight
+Alien.Sand.StepLeft
+Alien.Sand.StepRight

// These I could borrow from MvM
Humans.Pain
AlienCommando.Pain
AlienMedic.Pain
AlienDefender.Pain
AlienEscort.Pain
Humans.Death
AlienCommando.Death
AlienMedic.Death
AlienDefender.Death
AlienEscort.Death

Commando.BullRushFlesh // Commando touched an another team's player in bull rush (not the same as Adrenalin Rush?)
Commando.BullRushScream // Commando's bull rush sound start
Commando.BootSwing // Commando's boot melee attack
Commando.BootHit // Commando's boot melee attack

+Recon.WallJump // Recon hit a wall after air move

TFTeam.NotifyBaseUnderAttack // Self-explanatory. In fact, never should be played, judging by code
ClientModeTFNormal.ToggleMinimap // Minimap open/close
Minimap.ZoomIn // Minimap zoomed in
Minimap.ZoomOut // Minimap zoomed out
Player.WeaponSelectionMoveSlot // "cycle to next weapon" sound
Player.WeaponSelectionOpen // "open weapon selection" sound
//Player.DenyWeaponSelection // Weapon selection is not available. Exists in HL2


ResourceSpawner.BigPuff // Resource Spawner is spawning a resource chunk, or a random spawn effect
ResourceSpawner.Puff // Just a random spawn effect
+GasolineBlob.FlameSound // Flamethrower gasoline blob has been lit
ObjectSentrygun.Activate // Defender's sentrygun finished building
ObjectSentrygun.Idle // Defender's sentrygun random idle sound between rotations
ObjectSentrygun.FoundTarget // Defender's sentrygun has found target and has ammo
ObjectSentrygun.Turtle // Defender's sentrygun is starting to turtle (sliding into the ground)
ObjectSentrygun.UnTurtle // Defender's sentrygun is unturtling
ObjectSentrygun.Fire // Defender's sentrygun is firing
ObjectSentrygunRocketlauncher.Fire // Defender's sentrygun rocket launcher is firing
ObjectPortablePowerGenerator.Shutdown // Buff Station player detach sound, starts playing whenever a player disconnected from it
ObjectPortablePowerGenerator.Startup // Buff Station player attach sound
+MortarRound.StopSound // "Stop emitting smoke/sound". Should it be an empty sound then?
+MortarRound.IncomingSound // Play a fall sound when the mortar round begins to fall
NPC_Bug_Builder.Idle // The builder bug's idle sound, played in idle/alert state
NPC_Bug_Builder.Pain // The builder bug's pain sound
NPC_Bug_Warrior.AttackHit // The warrior bug's melee attack sound if hits a player ("Melee hit, one arm")
NPC_Bug_Warrior.AttackSingle // The warrior bug's melee start attack sound
NPC_Bug_Warrior.Idle // The warrior bug's idle sound, played in idle/alert state
NPC_Bug_Warrior.Pain // The warrior bug's pain sound
ResourceZone.AmbientActiveSound // Resource Zone is going active/inactive(out of resources/lost zone)
ObjectMannedMissileLauncher.Reload // Missile Launcher reloading ammo every 2.25(if has EMP powerup)/1.5 seconds
ObjectMannedMissileLauncher.Fire // Missile Launcher launches a rocket
ObjectResourcePump.UpgradeFailed // Not enough resources at player when upgrading Resource Pump
ObjectResupply.InsufficientFunds // Medic's resupply beacon doesn't have enough resources to activate it
ObjectSentrygun.ResupplyAmmo // Defender's sentrygun got an ammo from player (by "addammo" clientcommand)
ObjectTunnelTrigger.TeleportSound // Tunnel starts to teleport
ObjectTunnelTrigger.DisabledSound // Disabled tunnel reacts to player
ShieldGrenade.Bounce // Shield Grenade bounching (random-defined by soundscript)
ShieldGrenade.StickBeep // Shield Grenade beeps
VehicleBatteringRam.BashSound // Battering Ram vehicle collided with an object
+VehicleMortar.FireSound // Mortar vehicle created it's round
+VehicleTank.FireSound // Tank vehicle created it's round
+Skirmisher.GunChargeSound // Mini Strider walker stops walking and starts firing large gun
+Skirmisher.GunSound // Mini Strider walker shoots machine gun
+Skirmisher.Footstep // Mini Strider walker's feet hits the ground
+Brush.Fire // Strider walker stops walking and starts firing it's main weapon
+Brush.Footstep // Strider walker's feet hits the ground
BaseEMPableGrenade.Fizzle // EMPable grenade sparks for a 0.5 seconds when applying EMP damage
GrenadeEMP.Bounce // EMP grenade plays a distinctive grenade bounce sound to warn nearby players
LimpetMine.Beep // Limpet Mine beep sound when activated by Use
LimpetMine.Fizzle // Limpet Mine fizzles for 0.3 seconds when activated
+GrenadeObjectSapper.Arming // Sapper arming (on FinishAttaching)
+GrenadeObjectSapper.RemoveSapper // Enemy player removes the Sapper via Use
+GrenadeRocket.FlyLoop ?= weapons/rocket_flyloop1.wav // Rocket grenade plays loop sound when spawning
BasePlasmaProjectile.ShieldBlock // Plasma projectiles blocked by the Shield
ObjectMannedPlasmagun.Fire // A stationary plasma gun firing sound
+WeaponRepairGun.Healing = weapons/repair_gun/healing.wav // Arc Welder/Repair Gun healing something/Sapper's draining beam weapon sound
+WeaponRepairGun.NoTarget = weapons/repair_gun/notarget.wav // Repair Gun/Sapper's draining beam notarget sound
WeaponCombatShield.TakeBash // Player has been bashed by another player's shield
Harpoon.Impact // Harpoon touched something
Harpoon.Impale // Harpoon hits something moveable
Harpoon.HitFlesh // Harpoon's secondary attack, hits player
Harpoon.HitMetal // Harpoon's secondary attack, hits metal
Harpoon.Yank // Harpoon impaled something and pulling it
WeaponLimpetmine.Deny // "Don't allow anymore limpets to be placed if we've reached the max"





+vox/reinforcement.wav // (Alien) reinforcements arrived, called by an "impulse 150" command (previously, when alien player spawned, every 15 seconds)
vox/harvester-attack.wav // Carrier/harvester under attack (not being used)
vox/harvester-destroyed.wav // Carrier/harvester destroyed (not being used)
vox/new-tech-level.wav // New tech level open (not being used)
+vox/resource-zone-emptied.wav // Resource Zone emptied, played on both teams
vox/captured-alpha-resource.wav (not being used?)
vox/captured-beta-resource.wav (not being used?)
vox/captured-charlie-resource.wav (not being used?)
vox/resource-lost.wav (is that TFTeam.LostZone?)
+vehicles/diesel_start1.wav (already at HL2) // Battering Ram/Flatbed/Mortar/Motorcycle/Siege Tower/Tank/Teleport Station/Wagon vehicle start sound
+vehicles/diesel_loop2.wav (already at HL2) // Battering Ram/Flatbed/Mortar/Motorcycle/Siege Tower/Tank/Teleport Station/Wagon vehicle running sound
+vehicles/diesel_nitroboost.wav // Battering Ram/Motorcycle/Tank/Wagon vehicle turbo sound
weapons/carbine/carb_fire1.wav // Vehicle mounted machinegun fire sound 1
weapons/carbine/carb_fire2.wav // Vehicle mounted machinegun fire sound 2
weapons/mortar/mortar_click.wav // Plasma Grenade's (Grenade Anti Personnel), that has been shooted by Combat Plasma Grenade Launcher, bounce sound
+fire/burning1.wav // Flame Thrower sound


