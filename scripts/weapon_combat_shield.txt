// Combat Shield (Human)

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"Combat Shield (Human)"
	"viewmodel"			"models/weapons/v_combatshield.mdl"
	"playermodel"		"models/weapons/v_pistol.mdl"
	"anim_prefix"		"anim"
	"bucket"			"1"
	"bucket_position"	"1"

	"clip_size"			"0"
	"primary_ammo"		"None"
	"secondary_ammo"	"None"

	"weight"			"5"
	"item_flags"		"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"reload"			"Default.Reload"
		"empty"				"Default.ClipEmpty_Rifle"
		"single_shot"		"Weapon_USP.Single"
		"special1"			"Weapon_USP.SilencedShot"
		"special2"			"Weapon_USP.DetachSilencer"
		"special3"			"Weapon_USP.AttachSilencer"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/w_icons1"
				"x"		"128"
				"y"		"0"
				"width"		"128"
				"height"	"64"
		}
		"weapon_s"
		{
				"file"		"sprites/w_icons1b"
				"x"		"128"
				"y"		"0"
				"width"		"128"
				"height"	"64"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"			"55"
				"y"			"60"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}